﻿using Ejemplo01_Encapsulacion;
using Ejemplo02_Herencia;
using Ejemplo03_Interfaces;
using Ejercicio02_Herencia;
using System;

namespace Ejercicio03_interfaces
{
    /* 1 - Comprobar si Coche Electrico puede hacerse polimorfismo con INom...
     * 2 - Podeis un nuevo proyecto Ejercicio03_interfaces
     *      Hacer que el usuario que ya tenemos implemente la interfaz 
     *      INombrable y usar los 2 métodos y la propiedad
     * 3 -  Crear un array de INombrable con un empleado y su coche electrico
     * 4 -  ICloneable YA EXISTE EN .Net
     *      Implementar dicha interfaz en CocheElectrico:
     *      El método tiene que instanciar un nuevo obj y asignar 
     *      las propiedades del nuevo coche con sus propias propiedades.
     *      Es decir, se clona a sí mismo
     * 5 - Crear una nueva interfaz que obligue a implementar
     *      2 métodos, uno para mostrar directamente los datos por consola
     *      y otro para pedir sus datos por consola.
     * 6 -  Implementar dicha interfaz en Usuario y en Coche
     *      sobreescribir los métodos en Empleado (si quereis en CElectrico)
     * 7 -  Usar los métodos en un nuevo usuario y en el empleado del 
     *      ejercicio 3, y en un Coche (y si quereis CocheElectrico)
     */
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("INombrable!");
            INombrable[] varios = new INombrable[2];
            varios[0] = new Empleado("Pepito", 35, 1.8f, 20000);
            varios[1] = new CocheElectrico("BMW", "S2", 15000, 90);
            // Completamos la depencia unidireccional
            ((Empleado)varios[0]).SuCoche = (Coche) varios[1];

            foreach (INombrable innom in varios)
            {
                Console.WriteLine(innom.ToString());
            }

            CocheElectrico mismoElectrico = (CocheElectrico) varios[1];
            CocheElectrico copiaElectrico = (CocheElectrico) ((CocheElectrico)varios[1]).Clone();

            mismoElectrico.Modelo = "Nuevo modelo S2";
            Console.WriteLine("mismoElectrico = " + mismoElectrico.ToString());
            Console.WriteLine("varios[1] = " + varios[1].ToString());

            copiaElectrico.Modelo = "Copia nuevo modelo S2";
            Console.WriteLine("copiaElectrico = " + copiaElectrico.ToString());
            Console.WriteLine("varios[1] = " + varios[1].ToString());

            Empleado empleadoUsuario = new Empleado();
            empleadoUsuario.PedirDatos();
            empleadoUsuario.MostrarDatos();

            Coche cocheUsuario = new Coche();
            cocheUsuario.PedirDatos();
            cocheUsuario.MostrarDatos();
        }
    }
}
