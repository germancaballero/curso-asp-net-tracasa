﻿using Ejemplo06_CSharp_Avanzado.MetodosExt;
using OtroNamespace;
using System;

namespace Ejemplo06_CSharp_Avanzado
{
    class Program
    {
        static void Main(string[] args)
        {
            Fruta esLaPera = new Fruta("La pera repera", 150.01f);
            Console.WriteLine(esLaPera.ToString());
            Console.WriteLine(esLaPera.FormatearNombre());
            Console.WriteLine(FormatearNombreN(esLaPera));
            Console.WriteLine(esLaPera.FormatearNombre("Texto formato"));
            Console.WriteLine(esLaPera.ToUpperString());
            int[] otroArray = new int[] { 3, 2, 1 };
            Console.WriteLine(otroArray.ToUpperString());
            string[] otroArrayStr = new string[] { "tres", "dos", "uno" };
            Console.WriteLine(otroArrayStr.ToUpperString());

            OtraClase otroObj = new OtraClase();
            // Ejercicio: Método de extensión de array, que se array.ToStringFiltrado("texto");
            // Que sólo muestre los ToString() que contentan cadena, sin tener en cuenta mayus o minus.
            // Ej: otroArrayStr.ToStringFiltrado("s")   tres, dos
            // Ej: frutas.ToStringFiltrado("p")   esLaPera, Piña, Platano
            Fruta[] frutas = new Fruta[] { 
                esLaPera, 
                new Fruta("Manzana", 175), 
                new Fruta("Piña", 375), 
                new Fruta("Platano", 125), 
                new Fruta("Chirimoya", 275) };
            Console.WriteLine(frutas.ToUpperString());
        }
        public static string FormatearNombreN(Fruta laFruta)
        {
            return "FRUTA " + laFruta.Nombre.ToUpper();
        }
    }
}
namespace OtroNamespace
{
    class OtraClase
    {

    }
}
