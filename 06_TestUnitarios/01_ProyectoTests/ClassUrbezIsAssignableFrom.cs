﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_ProyectoTests
{
    class ClassUrbezIsAssignableFrom
    {
        // Úrbez JRQ

        // Enlace a la documentacion general:
        // https://docs.nunit.org/articles/nunit/writing-tests/assertions/classic-assertions/Assert.IsAssignableFrom.html

        // Tipos admitidos:
        // https://docs.nunit.org/articles/nunit/writing-tests/constraints/Constraints.html#type-constraints

        [Test(Author = " Úrbez JRQ")]
        public void TestIsAsignableFormConTipo()
        {

            Assert.IsAssignableFrom(typeof(int), 5, "5 no es de tipo int");
            Assert.IsNotAssignableFrom(typeof(int), new String(""));
        }

        // Ejemplo basado en la respuesta de https://stackoverflow.com/questions/5560449/simple-assert-isassignablefromt-failing
        public class Employee
        {
            public int EmployeeId { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public DateTime DateOfJoining { get; set; }
        }

        public class Manager : Employee
        {
            public IList<Employee> EmployeesReporting { get; set; }
        }

        public class testClass { 

        }
        object GetObjetoAsaber()
        {
            if ((new Random()).Next() % 2 == 0)
            {
                return new Employee();
            }
            else
            {
                return new Manager();
            }
        }

        [Test]
        public void TestIsAsignableFormSinTipo()
        {
            var employee = new Employee();
            var manager = new Manager();

            Assert.IsAssignableFrom<Manager>(employee);
            Assert.IsNotAssignableFrom<Employee>(manager);
            // Assert.IsAssignableFrom<Employee>(GetObjetoAsaber());
        }
    }
}
