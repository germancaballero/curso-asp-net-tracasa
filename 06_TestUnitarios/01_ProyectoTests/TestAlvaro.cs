﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_ProyectoTests
{
    class TestAlvaro
    {
        public class Point
        {
            public int X { get; }
            public int Y { get; }

             public Point(int x, int y) => (X, Y) = (x, y);

            /*public Point(int x, int y)    Es lo mismo que....
            {
                (X, Y) = (x, y);    // Deconstructing
                // var tupla = (1, 2, 3);
                X = x;
                Y = y;
            }*/
        }
        public class Coordenada : Point
        {
             public int Z { get; }
             public Coordenada(int x, int y, int z) : base(x, y) => (Z) = (z);
        }

        [Test(Author = "Álvaro L", Description = "Probando IsNotInstanceOf")]
        public void TestProbandoIsNotInstanceOf()
        {

            int tres = 3;
            float cuatro = 4.0f;

            string mensaje = "El objeto actual es una instancia del tipo";
            string mensaje3 = "El objeto es de tipo entero";
            string mensaje4 = "El objeto actual es una instancia del float";
            Assert.IsNotInstanceOf(typeof(bool), tres);
            Assert.IsNotInstanceOf(typeof(bool), tres, mensaje);
            Assert.IsNotInstanceOf(typeof(float), tres);

            // Diferencia entre int y Int16

            Assert.IsInstanceOf(typeof(int), tres, mensaje3);
            Assert.IsNotInstanceOf(typeof(Int16), tres, mensaje3);
            Assert.IsNotInstanceOf(typeof(short), tres, mensaje3);
            Assert.IsInstanceOf(typeof(float), cuatro,mensaje4);

            Point p = new Point(3, 2);
            Assert.IsInstanceOf(typeof(Point), p);


            Assert.IsNotInstanceOf<bool>(tres, mensaje);
            // Assert.IsNotInstanceOf<int>(tres, mensaje);
            Assert.IsNotInstanceOf<Int16>(tres, mensaje);

            // Assert.IsNotInstanceOf<string>("ejemplo");

            char[] a_example = new char[] { 'h', 'o', 'l', 'a' };
            Assert.IsNotInstanceOf<string>('e');
            Assert.IsNotInstanceOf<char>("e");
            Assert.IsNotInstanceOf<char>(a_example, "El array también lo considera");
            Assert.IsNotInstanceOf<int[]>(a_example, "El array también lo considera");

            Object t = new Object();
            Assert.IsNotInstanceOf<int>(t);
            Assert.IsInstanceOf<object>(t);

            Point x = new Coordenada(1, 2, 3);

            Assert.IsInstanceOf(typeof(Point), x);

        }
    }
}
