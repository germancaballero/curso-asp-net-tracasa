﻿using NUnit.Framework;
using NUnit.Framework.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_ProyectoTests
{
    class AlumnosTests
    {
        [Test(Author = "Josune S", Description = "Prueba de Assert isNaN")]
        public void TestIsNan_Correcto_Incorrecto()
        {
            double zero = 0;
            Assert.IsNaN(zero / zero, "Es un número");
            Assert.IsNaN(Math.Sqrt(-1), "Es un número");
            //  Assert.IsNaN(Math.Sqrt(1), "1 es un número");//Falla porque es un número
            //   Assert.IsNaN(80 + 40, "120 es un numero");//Falla porque es un número
            //   Assert.Inconclusive();//Si ha habido un error previo, no se ejecuta*/
        }
        //Inconclusive
        //Inconclusive test is a test for which you cannot determine the result.
        //For example, if you have a test that uses some kind of an external resource (Internet connection, for example). 

        [Test(Author = "Josune S", Description = "Prueba de Assert Inconclusive con mensaje")]
        public void TestIncloncusive_ConMensaje()
        {
            bool condicion = true;
            if (condicion)
                Assert.Inconclusive("Test inconclusive");
        }

        [Test(Author = "Joseba", Description = "Probando funciones para comprobar que no haya valores igual a cero o nulo")]
        public void Probando_NotZero_NotNull()
        {
            double pi = 3.1416;
            Assert.NotZero(pi, "El número es cero");
            Assert.NotZero(10 / 3, "El resultado es cero");
            Assert.NotZero(100);
            // Solo falla cuando el valor o el resultado de la opoeración pasados es 0
            // Assert.NotZero(24 % 2);
            // Assert.NotZero(0);
            object strObj = "Nuevo objeto";
            Assert.NotNull(strObj, "El objeto {​​​​​0}​​​​​ es nulo", strObj.ToString());
            strObj = null;
            // Falla cuando el objeto pasado es null
            //Assert.NotNull(strObj, "El objeto strObject es nulo");
        }
        [Test(Author = "Carlos A.", Description = "Probando que varios números son 0. Pruebo con entero, números decimales, operaciones dentro del método Assert.Zero y parseando strings")]
        public void testZero()
        {
            //QUITAR LAS PARTES COMENTADAS (ÉSTA SECCIÓN NO) PARA QUE EL TEST FALLE
            //Sólo traga números.

            int entero = 0;
            //entero = 8;
            Assert.Zero(entero, "El entero  no es cero");

            float decim = 0;
            //decim = 0.5f;
            Assert.Zero(decim, "El float no es cero");

            Assert.Zero(12 - 15 + 3, "La operación no da 0");
            //Assert.Zero(118 - 15 + 4000*2, "La operación no da 0");

            Assert.Zero(0.5 - 0.5, "La operación con números decimales no ha dado cero");
            Assert.Zero(Math.Round(-0.5f - 0.2f + 0.700001f, 5), "En cuanto se empieza a manejar decimales, por cosas de punto flotante, los decimales no son exactos en las últimas cifras");
            //Assert.Zero(0.1 - 0.9, "La operación fijo que no iva a dar 0");

            Assert.Zero(int.Parse("0"), "¿0 parseado como int tira error?"); //no tira error
            Assert.Zero(float.Parse("0"), "¿0 parseado como float tira error?"); //no tira error
        }
        [Test(Author = "Xabi J", Description = "Funcionamiento de Multiple y de Negative, se lanzan varias excepciones ")]
        public void TestFuncionamientoMultipleAndNegativeMal()
        {
            Assert.Pass();  // Comentar para probar
            Assert.Multiple(() =>
            {
                var X = 1;
                Assert.Negative(-1);
                Assert.Negative(1.0f, "1 no es negativo");
                Assert.Negative(0, "0 no es negativo");
                Assert.Negative('c', "Esto no es ni un numero");
                Assert.IsTrue(false, "Funciona también con otros asserts");
            });
        }
        [Test(Author = "Xabi J", Description = "Idem, pero aquí no se lanzan excepciones")]
        public void TestFuncionamientoMultipleAndNegativeBien()
        {
            Assert.Multiple(() =>
            {
                double x = -1.0;
                Assert.Negative(x);
            });
        }

        public class Point: IComparable
        {
            public int X { get; }
            public int Y { get; }

            public Point(int x, int y) => (X, Y) = (x, y);

            public int CompareTo(object obj)
            {
                Point punto2 = (Point) obj;
                return this.Mod > punto2.Mod ? 1 : (this.Mod > punto2.Mod ? -1 : 0);
            }
            double Mod
            {
                get
                {
                    return Math.Sqrt(X * X + Y * Y);
                }
            }
        }

        [Test(Author = "Mariuxi", Description = "Test que prueba si un objeto es menor (menor o igual) que otro. ")]
        public void ATestComparaDosValores()
        {
            int num1 = 6;
            int num2 = 4;
            string mensaje1 = "Este mensaje es opcional y aparece cuando numero 1 es mayor que numero 2";
            string mensaje2 = "Este mensaje es opcional y aparece cuando numero 1 es mayor que numero 2 y cuando los números son iguales";
            Assert.LessOrEqual(num2, num1, mensaje1);
            Assert.Less(num2, num1, mensaje2);
            Assert.Less("AAA", "BBB", mensaje2);
            //Assert.Greater();
            Point p1 = new Point(1, 1);
            Assert.LessOrEqual(p1, new Point(2, 2));
           
        }
        [Test(Author = "Paula", Description = "Assert.That: Se asegura de que una condición es verdadera. Si la condición es falsa, AssertionException.")]
        public void Test_Assert_That()
        {
            //Assert.That(bool condition);
            Assert.That(5 == 5);

            // Con mensaje de error si la condición no es true
            Assert.That(5 != 6, "Es falso");

            // Assert.That(myString, Is.EqualTo("Hello"));
            Assert.That("Hola", Is.EqualTo("Hello").Or.EqualTo("Hola"), "Pues no es un saludo");
            Assert.That("Hola".Equals("Hola"), "Vaya");
        }
        [Test(Author = "Xabier CV", Description = "Comprueba si cualquier variable de tipo numérico es positiva(int,float,double,uint,long,ulong y decimal)")]
        public void TestNumeroEsPositivoBien()
        {
            Assert.Positive(1);//int
            Assert.Positive(0.1f);//float
            Assert.Positive(0.2);//double
            Assert.Positive(2.1m);//decimal
            Assert.Positive('a');
        }

        [Test(Author = "Xabier CV", Description = "Comprueba si cualquier variable de tipo numérico es positiva(int,float,double,uint,long,ulong y decimal)")]
        public void TestNumeroEsPositivoMal()
        {
            Assert.Pass(); // Quitar para probar
            Assert.Positive(-'a');
            Assert.Positive(-1);
            Assert.Positive(0);
        }
        [Test(Author = "XavierUsu", Description = "Probando funcionalidad null and pass")]
        public void TestProbandoNullAndPass()
        {
            //Assert.Null("jjj","Holi", new object[1, 2] );
            Assert.Null(null);
            Assert.Pass("En el error?");
            // throw new SuccessException("Que curioso");
        }
        [Test]
        public void TestQueSeIgnora()
        {

            Assert.Ignore("Ignorado");
        }
        [Test(Author = "Sergio S", Description = "Probando Assert.Warn y Warn")]
        public void TestProbandoWarn()
        {
            // Lanza un mensaje tipo string que le podemos pasar como parámetro o introducir directamente,
            // pero sigue ejecutando los test, no se detiene. Tambien podemos usar Warn.If() ó Warn.Unless().
            string warning = "Advertencia";
            Assert.Warn(warning);
            Warn.If(2 + 2 != 4, "Sí ha devuelto false");
            Warn.If(() => 2 + 2 != 5, "No ha devuelto false");  // Con Warn.If lanza el warning cuando se cumple lo que indicamos
            Assert.Warn("Test advertencia");
            Warn.Unless(2 + 2, Is.EqualTo(4));
            // Assert.Ignore(warning); // Lanza el mensaje pero ignora todo el test, tanto lo que ha pasado antes como lo que viene después.
            Warn.Unless(() => 2 + 2, Is.EqualTo(5).After(3000));   // Con Warn.Unless lanza el warning cuando NO se cumple lo que indicamos        
        }
    }
}

