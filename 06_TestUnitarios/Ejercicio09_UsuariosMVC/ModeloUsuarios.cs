﻿using Ejemplo01_Encapsulacion;
using Ejemplo03_MVC;
using Ejercicio01_Encapsulacion;
using System;
using System.Collections.Generic;
using System.Text;

namespace ModeloUsuarios
{
    public class ModeloUsuarios
    {
        private IList<Usuario> listaUsuarios;
        static ModeloUsuarios instancia;

        public Action<Usuario> alCrear;
        public Action<Usuario> alEliminar;
        public Action<Usuario, Usuario> alModificar;
        // public Action<Usuario> alLeer;
        public Func<List<Usuario>> alInicializar;
        public static ModeloUsuarios Instancia 
        {
            get
            {
                if (instancia == null)
                    instancia = new ModeloUsuarios();
                return instancia;
            }
         }

        ModeloUsuarios()
        {
            this.listaUsuarios = new List<Usuario>();
            IntentarInicializar();
        }
        void IntentarInicializar()
        {
            if (alInicializar != null)
            {
                listaUsuarios = alInicializar?.Invoke();
                alInicializar = null;
            }
        }
        public IList<Usuario> LeerTodos()
        {
            IntentarInicializar();
            return listaUsuarios;   
        }
        public Usuario Modificar(string nombreBuscado, string nuevoNombre, int nuevaEdad, float nuevaAltura)
        {
            Usuario usuBuscado = LeerUno(nombreBuscado);
            Usuario nuevo = new Usuario(nuevoNombre, nuevaEdad, nuevaAltura);
            return Modificar(usuBuscado, nuevo);
        }
        public Usuario Modificar(Usuario buscado, Usuario modif)
        {
            IntentarInicializar();
            for (int contador = 0; contador<listaUsuarios.Count;contador++)
            {
                if (listaUsuarios[contador] == buscado)
                {
                    listaUsuarios[contador] = modif;
                    alModificar(buscado, modif);
                    return modif;
                }
            } 
            return null;
        }
        public Usuario LeerUno(string nombreBuscado)
        {
            Usuario buscado = new Usuario();
            buscado.Nombre = nombreBuscado;
            return LeerUno(buscado);
        }
        public Usuario LeerUno(Usuario buscado)
        {
            IntentarInicializar();
            foreach (Usuario usu in listaUsuarios)
            {
                if (usu.Nombre.ToLower() == buscado.Nombre.ToLower())
                {
                    return usu;
                }
            }
            return null;
        }
        public bool Eliminar(Usuario usu)
        {
            if (usu != null)
            {
                listaUsuarios.Remove(usu);
                alEliminar?.Invoke(usu);
                return true;
            }
            return false;
        }
        public bool Eliminar(string buscado)
        {
            IntentarInicializar();
            Usuario usu = LeerUno(buscado);
            return Eliminar(usu);
        }
        public Usuario Crear(string nombre, int edad, float altura)
        {
            Usuario usu = new Usuario(nombre, edad, altura);
            return Crear(usu);
        }
        public Usuario Crear(Usuario usu)
        {
            IntentarInicializar();
            listaUsuarios.Add(usu);
            alCrear?.Invoke(usu);
            return usu;
        }   
    }
}
