﻿-- https://www.w3schools.com/sql
-- Para evitar duplicados
-- SELECT DISTINCT edad FROM [dbo].[Usuario]
-- SELECT  COUNT(DISTINCT edad)   FROM [dbo].[Usuario]

-- select  * from [dbo].[Usuario] where edad > 40
-- select  * from [dbo].[Usuario] where activo = 'False'
-- select  * from [dbo].[Usuario] where activo = 0

-- select  * from [dbo].[Usuario] where activo = 1 AND altura < 2
-- select  * from [dbo].[Usuario] where activo = 0 OR EDAD >= 33
-- select  * from [dbo].[Usuario] where not(activo = 0 OR EDAD >= 33)

-- select  * from [dbo].[Usuario] order by Edad DESC, activo asc

 -- select  * from [dbo].[Usuario] where coche is not null

 -- Devolver los 5 usuarios de mayor edad sin coche, 
 --  select * from [dbo].[Usuario] order by edad asc

 -- select top 5  * from [dbo].[Usuario] where coche is null order by edad desc
    
 -- Devolver los 5 usuarios de mayor edad sin coche, 
 -- ordenados alfabéticamente por nombre: Necesaria subconsulta
/*  SELECT * FROM (
    select top 5  * from [dbo].[Usuario] where coche is null order by edad desc
 ) AS subconsulta_5 ORDER BY nombre*/

 /*SELECT Nombre, Edad, case WHEN edad < 18 then 'Menor de edad' ELSE 'Mayor de edad' END
 FROM Usuario*/
 -- Devolver cuantos usuarios hay inactivos y cuantos activos
/* SELECT COUNT(DISTINCT activos.Id), COUNT(DISTINCT inactivos.Id)
     FROM  (select * from usuario  where activo = 0) as activos, 
           (select * from usuario  where activo = 1) as inactivos
           
 SELECT COUNT(CASE activo when 1 then 1 end), COUNT(CASE activo when 0 then 1 end)
     FROM usuario*/

/*SELECT Activo, Count(activo) FROM Usuario GROUP BY Activo

SELECT COUNT(DISTINCT AC.ID) n_activos, COUNT(DISTINCT INAC.ID) n_inactivos
FROM USUARIO AC, USUARIO INAC
WHERE AC.activo = 1 AND INAC.activo = 0*/
/*
SELECT * 
FROM USUARIO AC, USUARIO INAC
WHERE AC.activo = 1 AND INAC.activo = 0 ORDER BY AC.Nombre, INAC.Nombre*/

/*SELECT * FROM (SELECT COUNT(*) AS Activos FROM Usuario WHERE Activo = 'True') AS UsuariosActivos,
		     (SELECT COUNT(*) AS Inactivos FROM Usuario WHERE Activo = 'False') AS UsuariosInactivos
 */
 -- Devuelve los 3  usuarios de menor altura ordenados por edad
 -- SELECT * FROM (SELECT TOP 3 * FROM Usuario ORDER BY altura ASC) AS usu_m_a ORDER BY EDAD

 -- Devuelve los usuarios con el mismo coche
 /*select * FROM Usuario as U1, Usuario as U2 where U1.Coche = U2.Coche AND U1.Id < U2.Id
    ORDER BY U1.Nombre, U2.Nombre */
/*
-- Devolver el usuario de más edad con coche
select max(edad) FROM Usuario WHERE Coche is not null

select top 1 * from Usuario WHERE Coche is not null order by edad desc 
-- Devolver el usuario de menos edad inactivo y sin conche

select min(edad) FROM Usuario WHERE Coche is null and activo = 0

select top 1 * from Usuario WHERE Coche is null and activo = 0 order by edad asc 
*/
-- SELECT max(edad) FROM Usuario WHERE activo = 1
/*
SELECT  COUNT(*) AS cantidad, 
        CASE WHEN Coche is not null then Coche ELSE 'Sin coche' END 
        As Coche
    FROM Usuario 
    GROUP BY Coche 
    ORDER BY cantidad
    */
-- Calcula la media de edades de los usuarios activos, inactivos, con coche y sin coche
/*SELECT avg(case when activo = 1 THEN CAST(Edad AS FLOAT) END) AS activos,
       avg(case when activo = 0 THEN CAST(Edad AS FLOAT) END) as inactivos,
       avg(case when coche is not null THEN CAST(Edad AS FLOAT) END) as con_coche,
       avg(case when coche is null THEN CAST(Edad AS FLOAT) END) as sin_coche FROM usuario 

SELECT avg(Edad)  FROM usuario WHERE activo = 1
SELECT avg(Edad)  FROM usuario WHERE activo = 0
SELECT avg(Edad)  FROM usuario WHERE coche is not null
SELECT avg(Edad)  FROM usuario WHERE coche is null

SELECT RAND(Id) from Usuario
*/

SELECT U.Id, U.Nombre FROM Usuario as U where U.Nombre LIKE 'L%'
SELECT U.Id, U.Nombre FROM Usuario as U where U.Nombre LIKE '%ez'
SELECT U.Id, U.Nombre, U.email FROM Usuario as U where U.email LIKE '%@ge%'