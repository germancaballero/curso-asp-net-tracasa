﻿-- El conjunto común de datos
/*
SELECT	U.Id, U.email, 
		C.Id, C.Marca
	FROM Usuario as U
	INNER JOIN Coche as C
	ON U.idCoche = C.Id
	*/
-- Todos los usuarios y si tienen coche, con su coche. Y si no, se rellena lo del coche como NULL
/*SELECT	U.Id, U.email, 
		C.Id, C.Marca
  FROM Usuario as U
			LEFT JOIN Coche as C
	ON U.idCoche = C.Id */

-- Todos los coches con sus usuarios si los tuviera, y si no también
/*SELECT	U.Id, U.email, 
	C.Id, C.Marca
	FROM Usuario as U
		RIGHT JOIN Coche as C
ON U.idCoche = C.Id
*/
-- Todo con todo aunque no tengan combinación

/*SELECT	U.Id, U.email, 
	C.Id, C.Marca
	FROM Usuario as U
		FULL OUTER JOIN Coche as C
ON U.idCoche = C.Id
*/



SELECT U.nombre, P.nombre 
	FROM Producto as P 
		INNER JOIN CompraUsuarioProducto as C	ON C.IdProducto = P.Id
		INNER JOIN Usuario AS U					On C.IdUsuario = U.Id
