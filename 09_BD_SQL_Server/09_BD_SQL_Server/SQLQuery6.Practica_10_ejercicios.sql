﻿/*	 select SUM(c.Cantidad) from Producto as P, CompraUsuarioProducto as C, Usuario as U 
			where C.IdUsuario = U.Id and C.IdProducto = P.Id and U.edad <= 20 
		1 - Esta consulta con JOIN
		
	Por cada usuario, nombre de usuario y nombre de producto,
		2 - El precio del producto de sus compras
		3 - Cuanto se ha gastado en total en cada producto
		4 - Cuantas copias del producto ha comprado
	Por otro lado:
		5 - Sumar todas las ventas (dinero) del 2020
		6 - Todos los usuarios que compraron en el 2019
		7 - Qué productos se han vendido mejor a lo largo del 2018, 2019 y 2020
		8 - El coche cuyo propietario ha comprado más a lo largo de la historia
		9 - La media de edad de los compradores de cada uno de los productos (melón, cuajada...)
		10 - Qué productos han comprado los usuarios del Mercedes */

-- 1)
/*select SUM(c.Cantidad) 
	from Producto as P	inner join CompraUsuarioProducto as C ON C.IdProducto = P.Id
						inner join Usuario as U on C.IdUsuario = U.Id 
						where U.edad <= 20	*/ 

-- 2)	Solución todo en 1
select Usuario.nombre as Usuario, subconsulta.Nombre as 'Producto comprado', subconsulta.precio, subconsulta.[TOTAL gastado en producto], subconsulta.[INSTANCIAS COMPRADAS], subconsulta.IdProducto
		from Usuario inner join
			(select idUsuario, IdProducto, Producto.PRECIO, sum(cantidad * Producto.PRECIO) as 'TOTAL gastado en producto', sum(cantidad) as 'INSTANCIAS COMPRADAS', Producto.Nombre
				from CompraUsuarioProducto inner join Producto on CompraUsuarioProducto.IdProducto = Producto.Id group by IdUsuario, IdProducto, Producto.Nombre, Producto.Precio) as subconsulta on Usuario.id = subconsulta.IdUsuario				
		ORDER BY Usuario

--	Por cada usuario, nombre de usuario y nombre de producto,
--		2 - El precio del producto de sus compras
SELECT DISTINCT U.nombre Usuario, P.Nombre Producto, P.Precio FROM Producto AS P 
RIGHT JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
LEFT JOIN Usuario AS U ON CUP.IdUsuario = U.Id ORDER BY Usuario
--		3 - Cuanto se ha gastado en total en cada producto
SELECT U.Id, U.nombre Usuario, P.Nombre Producto, sum( P.Precio*CUP.Cantidad  ) Gastado
FROM Producto AS P 
RIGHT JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
LEFT JOIN Usuario AS U ON CUP.IdUsuario = U.Id 
GROUP BY U.Id, P.Precio, P.Nombre, U.nombre ORDER BY Usuario
--		4 - Cuantas copias del producto ha comprado
SELECT U.nombre Usuario, P.Nombre Producto, SUM(CUP.Cantidad) Copias_Compradas 
FROM Producto AS P 
RIGHT JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
LEFT JOIN Usuario AS U ON CUP.IdUsuario = U.Id
GROUP BY U.nombre, P.Nombre
ORDER BY U.nombre

SELECT U.Nombre AS Usuario, P.Nombre AS Producto, C.Cantidad, P.Precio AS 'Precio unidad', 
	SUM(C.Cantidad * P.Precio) AS 'Precio total'
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C ON C.IdProducto = P.Id INNER JOIN Usuario AS U ON C.IdUsuario = U.Id
GROUP BY U.Nombre, C.Cantidad, P.Precio	,  P.Nombre
ORDER BY u.nombre

--5 - Sumar todas las ventas (dinero) del 2020
SELECT SUM(subconsulta.sumatorio) as 'Dinero sacado en el 2020' FROM (SELECT sum(cantidad*Producto.PRECIO) AS sumatorio
from CompraUsuarioProducto INNER JOIN Producto on CompraUsuarioProducto.IdProducto = Producto.id
WHERE year(CompraUsuarioProducto.Fecha) = 2020 group by IdProducto) as subconsulta
-- Sin subconsulta:  Valdría con INNER JOIN también
SELECT SUM(P.precio*UP.cantidad)'GASTADO'
FROM (Producto as P FULL 
	OUTER JOIN CompraUsuarioProducto as UP ON P.Id = UP.idProducto ) 
WHERE UP.fecha BETWEEN '2020-01-01' AND '2019-12-31 23:59:59'

SELECT SUM(CUP.Cantidad*P.Precio) Ventas FROM Producto AS P 
RIGHT JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
WHERE CUP.Fecha LIKE '%2020%'

--6 - Todos los usuarios que compraron en el 2019
	SELECT U.Nombre FROM Producto AS P
		INNER JOIN CompraUsuarioProducto AS C ON C.IdProducto = P.Id 
		INNER JOIN Usuario AS U ON C.IdUsuario = U.Id
		WHERE YEAR(Fecha) = 2019
		GROUP BY U.Nombre

	SELECT U.Id, U.nombre Usuario FROM CompraUsuarioProducto AS CUP
		LEFT JOIN Usuario AS U ON CUP.IdUsuario = U.Id
		WHERE CUP.Fecha LIKE '%2019%'
		GROUP BY U.Id, U.nombre
		
	SELECT DISTINCT U.Id, U.nombre Usuario FROM CompraUsuarioProducto AS CUP
		LEFT JOIN Usuario AS U ON CUP.IdUsuario = U.Id
		WHERE CUP.Fecha LIKE '%2019%'

--7 - Qué productos se han vendido mejor a lo largo del 2018, 2019 y 2020
-- CONSULTAS POR SEPARADO
SELECT TOP 1 P.Nombre AS 'Producto mas vendido 2018', SUM(C.Cantidad) AS total_ventas
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C ON C.IdProducto = P.Id INNER JOIN Usuario AS U ON C.IdUsuario = U.Id
WHERE YEAR(Fecha) = '2018' GROUP BY P.Nombre ORDER BY total_ventas DESC

SELECT TOP 1 P.Nombre AS 'Producto mas vendido 2019'
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C ON C.IdProducto = P.Id INNER JOIN Usuario AS U ON C.IdUsuario = U.Id
WHERE YEAR(Fecha) = '2019' GROUP BY P.Nombre ORDER BY SUM(C.Cantidad) DESC

SELECT TOP 1 P.Nombre AS 'Producto mas vendido 2020'
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C ON C.IdProducto = P.Id INNER JOIN Usuario AS U ON C.IdUsuario = U.Id
WHERE YEAR(Fecha) = '2020' GROUP BY P.Nombre ORDER BY SUM(C.Cantidad) DESC

-- UNA SOLA CONSULTA CON TODAS UNIDAS POR UNION

SELECT * FROM (SELECT TOP 1 '2018' AS Año, P.Nombre AS 'Producto mas vendido'
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C ON C.IdProducto = P.Id INNER JOIN Usuario AS U ON C.IdUsuario = U.Id
WHERE YEAR(Fecha) = '2018' GROUP BY P.Nombre ORDER BY SUM(C.Cantidad) DESC) AS X
UNION 
SELECT * FROM (SELECT TOP 1 '2019' AS Año, P.Nombre AS 'Producto mas vendido'
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C ON C.IdProducto = P.Id INNER JOIN Usuario AS U ON C.IdUsuario = U.Id
WHERE YEAR(Fecha) = '2019' GROUP BY P.Nombre ORDER BY SUM(C.Cantidad) DESC) AS Y
UNION 
SELECT * FROM (SELECT TOP 1 '2020' AS Año, P.Nombre AS 'Producto mas vendido'
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C ON C.IdProducto = P.Id INNER JOIN Usuario AS U ON C.IdUsuario = U.Id
WHERE YEAR(Fecha) = '2020' GROUP BY P.Nombre ORDER BY SUM(C.Cantidad) DESC) AS Z

-- UNA SOLA CONSULTA CON SUBCONSULTAS

SELECT *
FROM ( SELECT TOP 1 P.nombre, SUM(UP.CANTIDAD)'CANTIDAD VENDIDA 2018'
			FROM (Producto as P FULL OUTER JOIN CompraUsuarioProducto as UP ON P.Id = UP.idProducto ) 
			RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario
			WHERE UP.fecha BETWEEN '2018-01-01' AND '2018-12-31'
			GROUP BY P.nombre ORDER BY SUM(UP.CANTIDAD) DESC)			AS _2018,
	 ( SELECT TOP 1 P.nombre, SUM(UP.CANTIDAD)'CANTIDAD VENDIDA 2019'
			FROM (Producto as P FULL OUTER JOIN CompraUsuarioProducto as UP ON P.Id = UP.idProducto ) 
			RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario
			WHERE UP.fecha BETWEEN '2019-01-01' AND '2019-12-31'
			GROUP BY P.nombre
			ORDER BY SUM(UP.CANTIDAD) DESC)								AS _2019,
	 ( SELECT TOP 1 P.nombre, SUM(UP.CANTIDAD)'CANTIDAD VENDIDA 2020'
			FROM (Producto as P FULL OUTER JOIN CompraUsuarioProducto as UP ON P.Id = UP.idProducto ) 
			RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario
			WHERE UP.fecha BETWEEN '2020-01-01' AND '2020-12-31'
			GROUP BY P.nombre
			ORDER BY SUM(UP.CANTIDAD) DESC)								AS _2020

-- TRANSACT SQL

DECLARE @ANO AS INT
SET @ANO = 2018
WHILE @ANO <= 2020
BEGIN
	SELECT TOP 1		CompraUsuarioProducto.idProducto, 
						Producto.Nombre, 
						sum(cantidad) as copias_vendidas, 
						year(CompraUsuarioProducto.Fecha) as periodo 
			from CompraUsuarioProducto inner join Producto on CompraUsuarioProducto.IdProducto = Producto.Id
			group by idProducto, Producto.Nombre,  year(CompraUsuarioProducto.Fecha) 
			HAVING year(CompraUsuarioProducto.Fecha) = @ANO 
			ORDER BY copias_vendidas DESC
	SET @ANO = @ANO +1
END;

--8 - El coche cuyo propietario ha comprado más a lo largo de la historia
	-- En este caso si el usuario no tiene coche, no saca nada:
select Usuario.Id, Usuario.nombre, Coche.Marca, Coche.Modelo 
		from Usuario inner join coche on Usuario.idCoche= coche.Id 
		where Usuario.Id in (
					select top 1 CompraUsuarioProducto.IdUsuario
						from CompraUsuarioProducto as CUP
						group by IdUsuario
						ORDER BY SUM(Cantidad  ) DESC)
-- Esta solución muestra el que más ha comprado CON COCHE, y su coche
SELECT (C.Marca + ' ' + C.Modelo) AS 'Coche del usuario mas gastador'
	FROM Usuario AS U INNER JOIN Coche AS C ON U.IdCoche = C.Id
	WHERE U.Id IN
		(SELECT TOP 1 U.Id
				FROM Producto AS P 
				INNER JOIN CompraUsuarioProducto AS CUP ON CUP.IdProducto = P.Id 
				INNER JOIN Usuario AS U ON CUP.IdUsuario = U.Id
				WHERE U.IdCoche IS NOT NULL
				GROUP BY U.Id 
				ORDER BY SUM(P.Precio * CUP.Cantidad) DESC)
				
SELECT C.Marca, C.Modelo FROM (
		SELECT TOP 1 U.Id , SUM(P.Precio*C.Cantidad) AS 'PrecioTotal' 
				FROM Producto AS P 
				INNER JOIN CompraUsuarioProducto AS C ON P.Id = C.IdProducto 
				INNER JOIN Usuario AS U ON U.Id = C.IdUsuario
				WHERE U.idCoche IS NOT NULL 
				GROUP BY U.Id 
				ORDER BY PrecioTotal DESC) AS S, Coche AS C 
	INNER JOIN Usuario AS U ON C.Id =U.idCoche WHERE U.Id = S.Id

-- Muestra el coche del que más ha comprado, pero si no lo tiene, muestra null
SELECT TOP 1 C.Marca,C.Modelo
FROM ((Producto as P INNER JOIN CompraUsuarioProducto as UP ON P.Id = UP.idProducto ) RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario) LEFT JOIN Coche as C on C.id=U.idCoche
GROUP BY U.Id,C.Marca,C.Modelo
ORDER BY SUM(P.precio*UP.cantidad) DESC


--9 - La media de edad de los compradores de cada uno de los productos (melón, cuajada...)

	-- Transact-SQL:	
	--	Caluclo erróneo: Repite edades, el calculo no es del todo correcto porque usuarios repetidos 
	-- (que hayan comprado el mismo producto varias veces, cuentan más para la media.
DECLARE @aux_id_prod INT
DECLARE cur_u CURSOR FOR SELECT P.Id FROM Producto AS P
OPEN cur_u
FETCH NEXT FROM cur_u INTO @aux_id_prod

WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT IdProducto, P.Nombre, AVG(Edad) as edad_media
		FROM Usuario 
		INNER JOIN CompraUsuarioProducto AS CUP ON Usuario.Id = CUP.IdUsuario
		INNER JOIN Producto AS P ON P.Id = CUP.IdProducto
		GROUP BY IdProducto, P.Nombre
		HAVING IdProducto = @aux_id_prod
		FETCH NEXT FROM cur_u INTO @aux_id_prod
	END
CLOSE cur_u
DEALLOCATE cur_u

--- COnsulta única:
	--	Caluclo erróneo: Repite edades, el calculo no es del todo correcto porque usuarios repetidos 
	-- (que hayan comprado el mismo producto varias veces, cuentan más para la media.
SELECT P.Nombre Producto, AVG(CAST(U.Edad AS FLOAT)) Edad_Media_Compradores 
FROM Producto AS P 
RIGHT JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
LEFT JOIN Usuario AS U ON CUP.IdUsuario = U.Id
GROUP BY P.Nombre, P.Id
ORDER BY P.Id

-- Obligando a mostrar todos los productos, incluso los que no se han vendido
	--	Caluclo erróneo: Repite edades, el calculo no es del todo correcto porque usuarios repetidos 
	-- (que hayan comprado el mismo producto varias veces, cuentan más para la media.
SELECT P.Nombre Producto, 
	CASE WHEN AVG(CAST(U.Edad AS FLOAT)) IS NOT NULL 
		THEN  AVG(CAST(U.Edad AS FLOAT)) 
		ELSE 0 
	END			as Edad_Media_Compradores
FROM Producto AS P 
LEFT JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
LEFT JOIN Usuario AS U ON CUP.IdUsuario = U.Id
GROUP BY P.Nombre, P.Id
ORDER BY P.Id

-- Con subconsulta:
	-- OK: Necesario para no duplicar usuarios que hayan comprado el mismo producto varias veces.
SELECT idprod, subconsulta.nombreProd, AVG(subconsulta.edad) AS Media 
FROM (
	SELECT DISTINCT Usuario.edad as edad, Usuario.Id, CompraUsuPod.IdProducto as idprod, Producto.Nombre AS nombreProd
		FROM Usuario  
		INNER JOIN CompraUsuarioProducto AS CompraUsuPod ON Usuario.Id = CompraUsuPod.IdUsuario
		RIGHT JOIN Producto ON Producto.Id = CompraUsuPod.IdProducto
) AS subconsulta 
GROUP BY subconsulta.nombreProd, idprod
ORDER BY idprod

--10 - Qué productos han comprado los usuarios del Mercedes 

SELECT Usuario.nombre, Producto.Nombre FROM Usuario
INNER JOIN Producto ON Producto.Id = CompraUsuarioProducto.IdProducto
INNER JOIN CompraUsuarioProducto ON Usuario.Id = CompraUsuarioProducto.IdUsuario
INNER JOIN Coche ON Usuario.idCoche = Coche.Id
WHERE Coche.Marca='Mercedes'

--  Pruebas a parte

SELECT SUM(P.precio*UP.cantidad)'GASTADO'
FROM (Producto as P FULL 
	OUTER JOIN CompraUsuarioProducto as UP ON P.Id = UP.idProducto ) 
WHERE UP.fecha BETWEEN '2019-01-01' AND '2019-12-31 23:59:59'



SELECT *
FROM ( SELECT TOP 10 P.nombre, UP.CANTIDAD
			FROM (Producto as P FULL OUTER JOIN CompraUsuarioProducto as UP ON P.Id = UP.idProducto ) 
			RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario
			WHERE UP.fecha BETWEEN '2018-01-01' AND '2018-12-31'
			ORDER BY UP.CANTIDAD DESC)			AS _2018,
	 ( SELECT TOP 10 P.nombre,UP.CANTIDAD 'CANTIDAD VENDIDA 2019'
			FROM (Producto as P FULL OUTER JOIN CompraUsuarioProducto as UP ON P.Id = UP.idProducto ) 
			RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario
			WHERE UP.fecha BETWEEN '2019-01-01' AND '2019-12-31'
			ORDER BY  UP.CANTIDAD  DESC)								AS _2019,
	 ( SELECT TOP 10 P.nombre,  UP.CANTIDAD 'CANTIDAD VENDIDA 2020'
			FROM (Producto as P FULL OUTER JOIN CompraUsuarioProducto as UP ON P.Id = UP.idProducto ) 
			RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario
			WHERE UP.fecha BETWEEN '2020-01-01' AND '2020-12-31'
			ORDER BY  UP.CANTIDAD  DESC)								AS _2020
