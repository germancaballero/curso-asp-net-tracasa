﻿-- Cuantos usuarios tienen cada coche, con más de 2 usuarios:

SELECT C.Marca, COUNT( C.Marca)  
	from Coche as C 
		INNER JOIN Usuario as U On U.idCoche = c.Id
	group by C.Marca
	having COUNT( C.Marca)  >= 2