﻿
namespace Ejemplo06_Funcion_Calback
{
    public class Calculadora_B
    {
        //TODO: comprobar que un resultado, en cualquiera de las operaciones, no da un infinito
        static public float SumarB(float x, float y)        {   return x + y;   }

        static public float MultiplicarB(float x, float y)  {   return x * y;   }

        static public float RestarB(float x, float y)       {   return x - y;   }

        static public float DividirB(float x, float y)
        {
            // if (x / y == float.NegativeInfinity || x / y == float.PositiveInfinity)
            if (y == 0)
                throw new System.DivideByZeroException("Se trató de dividir por 0.");
            
            return x / y;
        }
    }
}
