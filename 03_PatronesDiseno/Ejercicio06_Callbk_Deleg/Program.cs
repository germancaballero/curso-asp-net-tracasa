﻿using System;
using System.Linq;

// Ejercicio: Crear dos sistemas (clases independientes 
// que no se conocen entre sí): 
// La primera, CalculaArrays, tendrá 4 funciones para sumar, rest, mult, div
// todo con float. Estas podrán realizar la operación sobre un array:
// Ej: {9, 7, 5, 3}  suma -> 24     resta ->  -6        mul -> 945
//     {42, 3, 2}   div -> (42 / 3) / 2 -> 7        suma -> 47
//      {3} ->   cualquier operador devuelve 3.
//      {66} ->   cualquier 0operador devuelve 66.
// No puede estar vacio el array
// La segunda clase, VistaCalc, le pide al usuario cuantos operandos habrá,
//  y los operandos uno a uno, la operación, y mostrará el resultado
// Si alguien quiere, en vez de eso, que resuelva: 3+22++11 ó 7*3*11*2
namespace Ejercicio06_Callbk_Deleg {
    class Program {
        static void Main(string[] args) {
            /*VistaCalc.PedirArray(CalculaArrays.CalculaArray);  */
            // VistaCalc.PedirArray(CalculaArrays.SumaArray, CalculaArrays.MultArray...);
            //string num = (1243432563.454545f * 5664334.5f).ToString("{ 0,10:0.0}");
            //Console.WriteLine(num);
            //float[] arr1 = { 1.1f, 2.2f, 3.3f };
            string[] strs = { "uno", "dos", "tres", "cuatro", "cinco" };
            Func<string, string, string> funSeparaComas = (acum, v) =>
            {
                Console.WriteLine("acum = " + acum + ", v = " + v);
                return acum.ToUpper() + ", " + v.ToUpper();
            };
            string result = strs.Aggregate(funSeparaComas);
            bool[] bools = { true, false, true, false };
            bool boolResult = bools.Aggregate((a, b) => a || b);
            Console.WriteLine("boolResult = " + boolResult);
            int[] ints = { 3, 4, 6, 7 };
            int intResult = ints.Aggregate((a, b) => a + b);
            Console.WriteLine("intResult = " + intResult);


            Console.WriteLine(result);

            float[] arr1 = {6.77777f };
            //  CalculaArrays.Sumar(null);
            while(true)
            {
                VistaCalc.PedirArray(CalculaArrays.CalculaArray);
                Console.ReadKey();
                Console.Clear();
            }
        }
    }
}
