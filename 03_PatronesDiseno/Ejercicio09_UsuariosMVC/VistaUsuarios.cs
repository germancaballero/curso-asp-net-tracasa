﻿using Ejemplo01_Encapsulacion;
using Ejercicio01_Encapsulacion;
using System;
using System.Collections.Generic;
using System.Text;

namespace ModeloUsuarios
{
    class VistaUsuarios
    {
        ControladorUsuarios controlador;
        public VistaUsuarios(ControladorUsuarios controlador)
        {
            this.controlador = controlador;
        }
        public void CrearUno()
        {
            InterfazConsola.PedirTexto("nombre de usuario: ", out string nombre);
            InterfazConsola.PedirInt("Edad: ", out int edad);
            InterfazConsola.PedirNum("Altura: ", out float altura);
            Usuario us = this.controlador.CrearUno(nombre, edad, altura);
            if (us != null)
            {
                Console.WriteLine("Creado nuevo usuario: " + us.ToString());
            }
            else
            {
                Console.WriteLine("No  se ha creado el usuario: " + nombre);
            }
        }

        public void EliminarUno()
        {
            InterfazConsola.PedirTexto("nombre a eliminar: ", out  string nombre);
            bool eliminado = controlador.EliminarUno(nombre);
            if (eliminado != false)
                Console.WriteLine("Usuario " + nombre + " eliminado");
            else
                Console.WriteLine("No encontrado usuario " + nombre);
        }

        public void ModificarUno()
        {
            InterfazConsola.PedirTexto("nombre a modificar: ", out string nombreBusq);
            InterfazConsola.PedirTexto("nuevo nombre: ", out string nombre);
            InterfazConsola.PedirInt("Nueva edad: ", out int edad);
            InterfazConsola.PedirNum("Nueva altura: ", out float altura);
            Usuario usMod = this.controlador.ModificarUno(nombreBusq, nombre, edad, altura);
            if (usMod != null)
                Console.WriteLine("Usuario " + usMod.ToString());
            else
                Console.WriteLine("No encontrado usuario " + nombreBusq);
        }

        public void MostrarTodos()
        {
            IList<Usuario> lista = controlador.MostrarTodos();
            if (lista.Count > 0)
                foreach (Usuario usu in controlador.MostrarTodos())
                {
                    Console.WriteLine(usu.ToString());
                }
            else
                Console.WriteLine("No hay usuarios");
        }

        public void MostrarUno()
        {
            InterfazConsola.PedirTexto("Usuario a mostrar (nombre)", out string nomBusq);
            Usuario usu = controlador.MostrarUno(nomBusq);
            if (usu != null)
                Console.WriteLine(usu.ToString());
            else
                Console.WriteLine("El usuario " + nomBusq + " no se ha encontrado");
            
        }


        public void Menu()
        {
            int resultado;
            do
            {
                Console.WriteLine("\n\nQUÉ DESEA HACER?");
                Console.WriteLine("1) Añadir uno");
                Console.WriteLine("2) Eliminar uno");
                Console.WriteLine("3) Modificar");
                Console.WriteLine("4) Mostrar uno");
                Console.WriteLine("5) Mostrar todos");
                Console.WriteLine("6) Salir");
                InterfazConsola.PedirNum("Elija una opción (de 0 a 6)", out resultado);

                switch (resultado)
                {
                    case 1:
                        CrearUno();
                        break;
                    case 2:
                        EliminarUno();
                        break;
                    case 3:
                        ModificarUno();
                        break;
                    case 4:
                        MostrarUno();
                        break;
                    case 5:
                        MostrarTodos();
                        break;
                    case 6:
                        break;
                }
            }
            while (resultado != 6);
        }
    }
}
