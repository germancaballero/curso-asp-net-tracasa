﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo10_PruebasPatrones
{
    class TestSelenium
    {
        IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            // driver = new ChromeDriver();
            string pathFirefox = "C:\\Users\\pmpcurso1\\AppData\\Local\\Mozilla Firefox\\firefox.exe";
            //var service = FirefoxDriverService.CreateDefaultService(pathFirefox);
            FirefoxBinary binary = new FirefoxBinary(pathFirefox);
            FirefoxOptions firefoxOptions = new FirefoxOptions();
            firefoxOptions.BrowserExecutableLocation = pathFirefox;
            driver = new FirefoxDriver(firefoxOptions);
            //FirefoxDriver fd = (FirefoxDriver) driver;
        }

        [Test]
        public void Test1()
        {
            driver.Navigate().GoToUrl("https://www.google.com/");
            IWebElement botonAcepto = driver.FindElement(By.Id("L2AGLb"));
            botonAcepto.Click();
            IWebElement ele = driver.FindElement(By.Name("q"));
            ele.SendKeys("javatpoint tutorials");
            IWebElement ele1 = driver.FindElement(By.Name("btnK"));
            //click on the search button  
            ele1.Click();
            // https://www.w3schools.com/css/css_attribute_selectors.asp
            var enlaces = driver.FindElements(By.CssSelector("a[href*='url']"));
            foreach (var enlace in enlaces)
            {
                enlace.Click();
                break;
            }
        }
    }
}
