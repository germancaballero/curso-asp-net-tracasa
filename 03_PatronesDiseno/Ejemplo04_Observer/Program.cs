﻿using System;

namespace Ejemplo04_Observer
{
    class Program
    {

        static void Main(string[] args)
        {
            MovidasObserver();
            Console.WriteLine(">>>>   Funcion terminada");
            // Invocar al Garbage Collector (recolector de basura)
            //      Busca objetos sin referencias, que no haya variables que apuntes a ellos
            //      y los elimina, libera su memoria de la RAM.
            System.GC.Collect();
            System.Threading.Thread.Sleep(1000);
        }
        static void MovidasObserver()
        {
            Console.WriteLine("Patron observer: Periodico");
            PeriodicoObservado alDia = new PeriodicoObservado();
            // 1 - Un suscriptor humano
            SuscriptorHumano juan = new SuscriptorHumano("Juan");
            alDia.AddSuscriptor(juan);
            // 2 - Ocurre una noticia
            alDia.NotificarNoticia("Un meteorito roza la Luna", DateTime.Now);
            // 3 - Otro suscriptor humano
            SuscriptorHumano ana = new SuscriptorHumano("Ana");
            alDia.AddSuscriptor(ana);
            // 4 - Otro suscriptor maquina, lleva un código int en vez de nombre
            SuscriptorMaquina cortocircuito = new SuscriptorMaquina(101110);
            alDia.AddSuscriptor(cortocircuito);
            // 5 - Ocurre otra noticia
            System.Threading.Thread.Sleep(2000);
            alDia.NotificarNoticia("EL meteorito destruye la Luna", DateTime.Now);
            // Noticia corazon:
            alDia.NotificarNoticiaCorazon("Una actriz beso a un fan", DateTime.Now);
            // 6 - El otro humano se desuscribe porque dice mucha mentira
            //      En el periodico tiene haber un método para quitar suscriptores
            alDia.RemoveSuscriptor(juan);

            System.Threading.Thread.Sleep(2500);
            // 7 - Ocurre la última noticia
            alDia.NotificarNoticia("EL meteorito NO destruyo la Luna, inocentes", DateTime.Now);
            // 8 - El periodico cierra
            // alDia = null;
            // 9 - Que los suscriptores reciban también la fecha y hora de la noticia
            //     y la muestren, claro
            Console.WriteLine(">>>>   Terminando funcion");

            // Para que el suscripctor de ciencia se pueda suscribir, simplemente se 
            // asigna la función al campo delegado:
            alDia.NuevaNotCiencia = NationalGeo;
            // Cuando realmente ocurre la noticia de ciencia:
            alDia.NoticiaCiencia("El meteorito existia, y era de criptonita!");
        }
        // Aquí las variables locales (primitivas, structs o referencias a obj) se destruyen, 
        // Pero no los objetos en sí, esos quedan vivos en alguna parte, pero si nadie que los recuerde
        
        // Este suscriptor sólo es una función
        static void NationalGeo(string noticiaDePseudociencia)
        {
            Console.WriteLine(">>>> A saber: " + noticiaDePseudociencia);
        }
    }
}
