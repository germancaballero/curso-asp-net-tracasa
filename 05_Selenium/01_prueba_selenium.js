const {Builder, By, Key, until, Options} = require('selenium-webdriver');
const firefox = require('selenium-webdriver/firefox');

(async function example() {
  
  let options = new firefox.Options();
  options.setBinary("C:\\Users\\pmpcurso1\\AppData\\Local\\Mozilla Firefox\\firefox.exe"); 

  let driver = await new Builder().forBrowser('firefox').setFirefoxOptions(options).build();
  console.log("let driver = await new Builder().");
  
  try {
    await driver.get('https://duckduckgo.com/', 3000);
    await driver.findElement(By.name('q')).sendKeys('webdriver', Key.RETURN);
    await driver.wait(until.titleIs('webdriver at DuckDuckGo'), 10000);
  } finally {
   await driver.quit();
  }
})();