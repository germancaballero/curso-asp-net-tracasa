﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace UsuariosWeb_APIRest.Models
{
    public class Producto
    {
        [Required]
        // [Key]
        public int Id { get; set; }

        [Required]
        public string Nombre { get; set; }

        [Required]
        // [Column(TypeName = "decimal(18, 2)")]
        [DataType(DataType.Currency)]
        public decimal Precio { get; set; }
    }
}