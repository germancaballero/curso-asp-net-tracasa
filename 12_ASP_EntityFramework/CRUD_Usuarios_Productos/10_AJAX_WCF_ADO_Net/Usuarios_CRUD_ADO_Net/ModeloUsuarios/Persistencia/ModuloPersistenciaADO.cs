﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.Json;

using System.Text.Json.Serialization;

namespace ModeloUsuarios
{
    public class ModuloPersistenciaADO
    {
        string pathBD = Path.GetFullPath("../../BD/bd_usu.mdf");

        string CONEX_BD;
        static Entorno entorno = Entorno.Desarrollo;
        ModeloUsuario modelo;

        List<Usuario> listaUsuarios = new List<Usuario>();

        public ModuloPersistenciaADO(Entorno entorno)
        {
            CONEX_BD = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=" + pathBD + ";Integrated Security=True;Connect Timeout=30";
            ModuloPersistenciaADO.entorno = entorno;

            ModeloUsuario.Instancia.delegadoCrear = Crear;
            ModeloUsuario.Instancia.delegadoEliminar = Eliminar;
            ModeloUsuario.Instancia.leerTodo = LeerTodo;
            ModeloUsuario.Instancia.leerUno = LeerUno;
            ModeloUsuario.Instancia.leerConfirmar = LeerConfirmar;
            ModeloUsuario.Instancia.modificarConId = Modificar;
            ModeloUsuario.Instancia.limpiar = Limpiar;
            listaUsuarios = Leer();
        }

        public void Limpiar()
        {
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {

                    conexion.Open();

                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "DELETE FROM Usuario WHERE id>=23";
                    int filasAfectadas = comando.ExecuteNonQuery();
                    if (filasAfectadas != 1)
                        throw new Exception("No ha hecho " + comando.CommandText);

                } //conexion.Close()
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
            }
        }

        public void Crear(Usuario usuario)
        {
            bool existe = false;
            foreach (Usuario usu in LeerTodo())
            {
                if (usu.Nombre == usuario.Nombre)
                {
                    existe = true;
                    break;
                }
                else
                {
                    existe = false;

                }
            }
            if (!existe)
            {
                Guardar(usuario);
            }
        }

        public void Guardar(Usuario usuario)
        {
            //TODO: Guardar en base de datos: ejecutar INSERTS
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();
                    int activo = 0;
                    if (usuario.Activo)
                        activo = 1;
                    SqlCommand comando = conexion.CreateCommand();

                    comando.CommandText = "INSERT INTO Usuario (nombre,email,altura,edad,activo) VALUES (@nombre, @email, @altura, @edad, @activo);";

                    comando.Parameters.AddWithValue("@nombre", usuario.Nombre);
                    comando.Parameters.AddWithValue("@email", usuario.Nombre.ToLower().Replace(' ', '_') + "@email.es");
                    comando.Parameters.AddWithValue("@edad", usuario.Edad);
                    comando.Parameters.AddWithValue("@altura", usuario.Altura.ToString().Replace(',', '.'));
                    comando.Parameters.AddWithValue("@activo", activo);

                    int filasAfectadas = comando.ExecuteNonQuery();
                    if (filasAfectadas != 1)
                        throw new Exception("No ha hecho insert bbdd " + comando.CommandText);
                    else
                    {
                        Usuario nuevousuario = LeerUno(usuario.Nombre.ToLower().Replace(' ', '_') + "@email.es");
                        listaUsuarios.Add(nuevousuario);
                    }
                } //conexion.Close()
                //Console.WriteLine("Guardados en bbdd: " + listaUsuarios.Count);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error guardando bbdd: " + ex.Message);
            }
        }


        public bool Eliminar(int id)
        {
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {

                    conexion.Open();

                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "DELETE FROM Usuario WHERE id = @id;";
                    comando.Parameters.AddWithValue("@id", id);

                    int filasAfectadas = comando.ExecuteNonQuery();
                    if (filasAfectadas != 1)
                        throw new Exception("No ha hecho " + comando.CommandText);

                } //conexion.Close()
                foreach (Usuario usu in listaUsuarios)
                {
                    if (usu.Id == id)
                    {
                        listaUsuarios.Remove(usu);
                        return true;
                    }
                }
                
                return false;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
                return false;
            }
        }
        public List<Usuario> Leer()
        {
            listaUsuarios = new List<Usuario>();
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();

                    Usuario usuario;
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "SELECT id,nombre, edad, altura,activo from Usuario;";
                    SqlDataReader lectorDR = comando.ExecuteReader();

                    while (lectorDR.Read())
                    {
                        usuario = new Usuario();
                        usuario.Id = lectorDR.GetInt32(0);
                        usuario.Nombre = lectorDR[1].ToString();
                        usuario.Edad = lectorDR.GetByte(2);
                        usuario.Altura = (float)(double)lectorDR[3];
                        usuario.Activo = lectorDR.GetBoolean(4);
                        listaUsuarios.Add(usuario);
                    }
                } //conexion.Close()
                Console.WriteLine("Leídos en bbdd: " + listaUsuarios.Count);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
            }
            return listaUsuarios;
        }

        public IList<Usuario> LeerTodo()
        {
            return listaUsuarios;
        }

        public Usuario LeerUno(int id)
        {
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();

                    Usuario usuario;
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "SELECT id,nombre, edad, altura,activo from Usuario WHERE id = @id;";
                    comando.Parameters.AddWithValue("@id", id);
                    SqlDataReader lectorDR = comando.ExecuteReader();

                    usuario = new Usuario();
                    Console.WriteLine(lectorDR.Read().ToString());
                    usuario.Id = lectorDR.GetInt32(0);
                    usuario.Nombre = lectorDR[1].ToString();
                    usuario.Edad = lectorDR.GetByte(2);
                    usuario.Altura = (float)(double)lectorDR[3];
                    usuario.Activo = lectorDR.GetBoolean(4);
                    return usuario;
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
                return null;
            }

        }
        public Usuario LeerUno(string email)
        {
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();

                    Usuario usuario;
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "SELECT id,nombre, edad, altura,activo from Usuario WHERE email = @email;";
                    comando.Parameters.AddWithValue("@email", email);
                    SqlDataReader lectorDR = comando.ExecuteReader();

                    usuario = new Usuario();
                    Console.WriteLine(lectorDR.Read().ToString());
                    usuario.Id = lectorDR.GetInt32(0);
                    usuario.Nombre = lectorDR[1].ToString();
                    usuario.Edad = lectorDR.GetByte(2);
                    usuario.Altura = (float)(double)lectorDR[3];
                    usuario.Activo = lectorDR.GetBoolean(4);
                    return usuario;
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
                return null;
            }

        }

        public bool LeerConfirmar(string nombre)
        {
            int posicion = 0;
            for (int i = 0; i < listaUsuarios.Count; i++)
            {
                if (listaUsuarios[i].GetNombre() == nombre)
                {
                    posicion = i;
                    return true;
                }
            }
            return false;
        }

        public void Modificar(int id, Usuario usu)
        {
            if (usu != null)
            {
                try
                {
                    SqlConnection conexion;

                    using (conexion = new SqlConnection(CONEX_BD))
                    {
                        conexion.Open();

                        SqlCommand comando = conexion.CreateCommand();
                        comando.CommandText = "UPDATE Usuario SET nombre = @nombre, edad = @edad, altura = @altura WHERE id = @id;";
                        comando.Parameters.AddWithValue("@id", usu.Id);
                        comando.Parameters.AddWithValue("@nombre", usu.Nombre);
                        comando.Parameters.AddWithValue("@edad", usu.Edad);
                        comando.Parameters.AddWithValue("@altura", usu.Altura.ToString().Replace(',', '.'));
                        //comando.Parameters.AddWithValue("@activo", usu.Activo);

                        int filasAfectadas = comando.ExecuteNonQuery();
                        if (filasAfectadas != 1)
                            throw new Exception("No ha hecho " + comando.CommandText);

                    } //conexion.Close()
                    listaUsuarios.Remove(usu);
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
                }
            }
        }

        public void ModificarSinId(Usuario usu)
        {
            Usuario usuario = LeerUno(usu.Nombre.ToLower().Replace(' ', '_') + "@email.es");
            int? id = usuario.Id;
            if (usuario != null)
            {
                try
                {
                    SqlConnection conexion;

                    using (conexion = new SqlConnection(CONEX_BD))
                    {
                        conexion.Open();

                        SqlCommand comando = conexion.CreateCommand();
                        comando.CommandText = "UPDATE Usuario SET nombre = @nombre, edad = @edad, activo = @activo, altura = @altura WHERE id = @id;";
                        comando.Parameters.AddWithValue("@id", usuario.Id);
                        comando.Parameters.AddWithValue("@activo", usuario.Activo);
                        comando.Parameters.AddWithValue("@nombre", usu.Nombre);
                        comando.Parameters.AddWithValue("@edad", usu.Edad);
                        comando.Parameters.AddWithValue("@altura", usu.Altura.ToString().Replace(',', '.'));
                        int filasAfectadas = comando.ExecuteNonQuery();
                        if (filasAfectadas != 1)
                            throw new Exception("No ha hecho " + comando.CommandText);

                    } //conexion.Close()
                    listaUsuarios.Remove(usu);
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
                }
            }
        }
    }
}