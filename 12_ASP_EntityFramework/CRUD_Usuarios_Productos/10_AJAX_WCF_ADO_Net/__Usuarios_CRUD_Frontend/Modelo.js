class Modelo {
    usuarios = [];
    constructor(usuarios){
        this.usuarios = usuarios;
    }
    mostrarTabla = null;
    comprobarTipo(nombre, edad, altura)
    {
        if (typeof(nombre)==="string" && !nombre =="" && isNaN(parseInt(nombre)))
        {
            if(!isNaN(parseInt(edad)))
            {
                if(!isNaN(parseFloat(altura)))
                {
                    return true;
                }
               
            }
        }
        else
        return false;
    }
    crearUsuario(nombre, edad, altura, activo, id) {

        let opcionesPOST = {
            method: "POST",
            mode: "cors",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json"
            },
            body: `{
            "Nombre": "${nombre}",
            "Edad": ${edad},
            "Altura": ${altura},
            "Activo": ${activo}}`
        }

        if(this.comprobarTipo(nombre,edad,altura))
        {
            fetch("./users", opcionesPOST)
            .then(respuesta =>  {
                console.log(respuesta.body);
                return respuesta.json(); 
            })
            .then(user => {
                console.log(user);
                this.usuarios.push(user);
                this.mostrarTabla(this.usuarios);                
            }).catch(err => console.error(err));
        }else{
            console.log("no se crea usuario");
        }
              

    }
    

    devolverId() {
        if(JSON.parse(window.localStorage.getItem("usuarios")) != null) {
            this.usuarios = JSON.parse(window.localStorage.getItem("usuarios"));

        } else {
            this.usuarios = [];

        }
        if(this.usuarios.length == 0) {
            return -1;
        } else {
            return this.usuarios[this.usuarios.length - 1].id;
        }
    }

    listaUsuarios() {
        let promesaAJAX = fetch("/api/users/");
            promesaAJAX
            .then((respuesta) => respuesta.json())
            .then(usuarios => {
                this.usuarios = usuarios;
                this.mostrarTabla(usuarios);
        });
        return this.usuarios;
    }

    modificarUsuario(nombre, edad, altura, activo, id) {
        
        let opcionesPUT = {
            method: "PUT",
            mode: "cors",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json"
            },
            body: `{
            "Nombre": "${nombre}",
            "Edad": ${edad},
            "Altura": ${altura},
            "Activo": ${activo} }`
        }
        if(this.comprobarTipo(nombre,edad,altura))
        {
            let promesaAJAX = fetch("/api/users/" + id, opcionesPUT);
        promesaAJAX
            .then((respuesta) =>
                respuesta.json())
            .then(condicion => {
                if (condicion == true) {
                    for (const usu of this.usuarios) {
                        if(id == usu.id) {
                            usu.nombre = nombre;
                            usu.edad = edad;
                            usu.altura = altura;
                            usu.activo = activo;
                            usu.id = id;
                        }
                    }
                    this.mostrarTabla(this.usuarios);
                }
            }
            );
        }else{
            console.log("no se crea usuario");
        }
        
            

    }

    eliminarUsuario(idDel) {
        let pos;
        for(let i = 0;i<this.usuarios.length;i++){
            if(this.usuarios[i].id == idDel)
                pos = i
        }

        let opcionesDEL = {
            method: "DELETE",
            mode: "cors",
            cache: "no-cache",
            headers: {
                'Content-Type':'application/json',
            }
        }
        let promesaAJAX = fetch("/api/users/" + idDel, opcionesDEL);
            promesaAJAX
            .then((respuesta) => 
                respuesta.json())
            .then(condicion => {
                if (condicion == true) { 
                    this.usuarios.splice(pos, 1); 
                    this.mostrarTabla(this.usuarios); 
                }
            }
        );
    }
    
}