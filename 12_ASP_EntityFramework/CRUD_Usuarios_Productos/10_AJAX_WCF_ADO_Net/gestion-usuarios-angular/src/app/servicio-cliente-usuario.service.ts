import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Usuario } from './usuario';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServicioClienteUsuarioService {

  private arrayUsuarios: Array<Usuario> = [];
  private url: string = "http://127.0.0.1:5000/api/users";

  constructor(public clienteHTTP: HttpClient) { 
    let observable: Observable<Array<Usuario>>;
    observable = this.clienteHTTP.get<Array<Usuario>>(this.url)
    observable.subscribe((datos: Array<Usuario>) => {
      this.arrayUsuarios = datos;
    });
  }

  public usuariosArray(): Array<Usuario> {
    return this.arrayUsuarios;
  }

  public cuantosUsuariosActivos(): number{
    let cuantosActivos = 0;
    this.usuariosArray().forEach(element => {
      if (element.activo == true) {
        cuantosActivos++;
      }
    });
    return cuantosActivos;
  }

  public cuantosUsuariosInactivos(): number{
    let cuantosInactivos = 0;
    this.usuariosArray().forEach(element => {
      if (element.activo == false) {
        cuantosInactivos++;
      }
    });
    return cuantosInactivos;
  }

  public cuantosUsuariosMayores(): number {
    let cuantosMayores = 0;
    this.usuariosArray().forEach(element => {
      if (element.edad >= 18) {
        cuantosMayores++;
      }
    });
    return cuantosMayores;
  }

  public cuantosUsuariosMenores(): number {
    let cuantosMenores = 0;
    this.usuariosArray().forEach(element => {
      if (element.edad < 18) {
        cuantosMenores++;
      }
    });
    return cuantosMenores;
  }
}
