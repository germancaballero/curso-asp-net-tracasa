﻿using System;

namespace Ejercicio01_Max
{
    class Program
    {
        static void Main(string[] args)
        {
            string entero = "null";

            Console.WriteLine("entero: " + (entero ?? "vacio"));

            int x = 3;
            switch (x)
            {
                case 3:
                    Console.WriteLine("3");
                    break;
                case 4:
                case 5:
                    Console.WriteLine("4 ó 5");
                    break;
                default:
                    Console.WriteLine("otro valor");
                    break;
            }

            int max = int.MinValue, min = int.MaxValue, num1;
            int cantidad = 5, contador = 0;
            string linea;

            while (contador < cantidad)
            {
                Console.WriteLine("Ingrese valor:");
                num1 = int.Parse(Console.ReadLine());
                if (num1 > max)  max = num1;
                //if (num1 < min)  min = num1;
                min = num1 < min ? num1 : min;
                /*if (num1 < min)
                    min = num1;
                else
                    min = min;*/
                contador++;
            }
            Console.WriteLine("max: " + max);
            Console.WriteLine("min: " + min);


        }
    }
}
