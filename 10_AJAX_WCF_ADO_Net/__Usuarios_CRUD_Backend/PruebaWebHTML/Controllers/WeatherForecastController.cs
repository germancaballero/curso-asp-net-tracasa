﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
// using System.Web.Http.Cors;

namespace Usuarios_CRUD_WCF.Controllers
{
    //[EnableCors("MyPolicy")]
    [ApiController]
    [Route("api/[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }
        // OPTIONS: api/TodoItems2 
       /* [EnableCors]
        [HttpOptions]
        public IActionResult PreflightRoute()
        {
            return NoContent();
        }*/
       // [EnableCors]
        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpPost]
        public WeatherForecast Post([FromBody] WeatherForecast bodyDelJSON)
        {
            if (bodyDelJSON.TemperatureC > 6000)
                throw new ArgumentException("Te has pasado de temperatura");

            var rng = new Random();
            bodyDelJSON.TemperatureC = bodyDelJSON.TemperatureC + 1 + rng.Next(0, 100);
            bodyDelJSON.Date = DateTime.Now;
            Console.WriteLine("Funciono: " + bodyDelJSON.ToString());
            return bodyDelJSON;
        }
    }
}
