﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Practica02_PatronesDeDiseno;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ejemplo_03_API_Rest.Controllers
{
    [ApiController]
    [Route("usus")]
    public class UsuariosYeahController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<UsuariosYeahController> _logger;

        public UsuariosYeahController(ILogger<UsuariosYeahController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Usuario> Get()
        {
            var rng = new Random();
            Usuario[] usuarios = new Usuario[]
            {
                new Usuario("aaaa", 10, 1),
                new Usuario("bbb", 22, 2),
                new Usuario("ccccc", 33, 3),
            };
            return usuarios;
        }
    }
}
