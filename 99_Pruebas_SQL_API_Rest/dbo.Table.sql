﻿CREATE TABLE [dbo].[Item]
(
	[Id] INT NOT NULL PRIMARY KEY identity(1,1), 
    [Name] VARCHAR(50) NULL, 
    [Designation] VARCHAR(50) NULL, 
    [City] VARCHAR(50) NULL, 
    [State] VARCHAR(50) NULL, 
    [Country] VARCHAR(50) NULL
)
