﻿using Practica02_PatronesDeDiseno;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace _02_Ejemplo_ADO_Net
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Pruebas ADO.Net");
            string dataSource = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\BD\\dbu.mdf;Integrated Security=True;Connect Timeout=10";
            // Authentication=Windows Authentication";
         /*   var constructorCadConex = new SqlConnectionStringBuilder
            {
                DataSource = dataSource
            };
            string cadenaConexion = constructorCadConex.ConnectionString;

            */

            SqlCommand comando = null;
            SqlConnection conexion;
            Usuario persona;
            List<Usuario> listaUsuarios = null;
            try
            {
                if (listaUsuarios == null) listaUsuarios = new List<Usuario>();
                using (conexion = new SqlConnection(dataSource))
                {
                    //  await conexion.OpenAsync();
                    conexion.Open();
                    comando = conexion.CreateCommand();
                    comando.CommandText = "SELECT nombre, email, anio, nacional, genero FROM usuarios ORDER BY nombre";
                    SqlDataReader lectorDB = comando.ExecuteReader();
                    while (lectorDB.Read())
                    {
                        persona = new Usuario();
                        persona.Nombre = lectorDB[0].ToString();
                        persona.Edad = lectorDB.GetInt32(1);
                        persona.Altura = lectorDB.GetFloat(2);
                        listaUsuarios.Add(persona);
                    }
                    lectorDB.Close();
                }
            }
            catch (Exception ex)
            {
                //this.EstadoBD?.Invoke(false, "ERROR EN IMPORTACION: " + ex.GetType().ToString() + ", " + ex.Message + ", " + ex.StackTrace);
                //return null;
                Console.WriteLine("Fallo: " + ex.Message);
            }
            finally
            {
                //this.EstadoBD?.Invoke(true, string.Format("IMPORTACION OK: {0} elementÓn", listaUsuarios.Count));
            }
            listaUsuarios.ForEach(Console.WriteLine);
        }
    }
}
