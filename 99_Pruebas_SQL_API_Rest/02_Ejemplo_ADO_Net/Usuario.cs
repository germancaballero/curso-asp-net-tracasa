﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica02_PatronesDeDiseno
{
    public class Usuario 
    {

        string nombre;
        float altura;
        int edad;

        public Usuario(string nombre , int edad, float altura)
        {
            this.nombre = nombre;
            this.altura = altura;
            this.edad = edad;
        }

        public Usuario()
        {
            this.nombre = "nada";
            this.altura = 1.5f;
            this.edad = 20;
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value == "" ? "vacio" : (value ?? "nulo"); }
        }

        public float Altura
        {
            get { return altura; }
            set { altura = value < 0.1f ? 0.1f : value; }
        }

        public int Edad
        {
            get { return edad; }
            set { edad = value < 1 ? 1 : value; }
        }

        public override string ToString()
        {
            return Nombre + " " + Altura.ToString() + " " + Edad.ToString();
        }

        public string GetNombre()
        {
            throw new NotImplementedException();
        }

        public void SetNombre(string nombre)
        {
            throw new NotImplementedException();
        }

        public void MostrarDatosConsola()
        {
            Console.WriteLine(this.ToString());
        }

    }
}
