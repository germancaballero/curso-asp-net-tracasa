﻿CREATE TABLE [dbo].[Usuarios]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [nombre] NCHAR(60) NOT NULL, 
    [edad] INT NOT NULL, 
    [altura] FLOAT NULL, 
    [activo] BIT NOT NULL
)
