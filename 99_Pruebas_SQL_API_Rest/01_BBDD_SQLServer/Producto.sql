﻿CREATE TABLE [dbo].[Producto]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Nombre] NCHAR(20) NOT NULL, 
    [Precio] FLOAT NULL, 
    [Cliente] INT NULL
)
